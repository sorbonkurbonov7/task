@if ($errors->any())
<div class="container" style="margin-top: 1rem">
  <div class="row">
    <div class="col-md-7">
      <div class="alert alert-danger">
      <ul class="ml30">
        @foreach ($errors->all() as $error)
        <li> {{$error}} </li>
        @endforeach
      </ul>
      </div>
    </div>
  </div> 
</div>
@endif